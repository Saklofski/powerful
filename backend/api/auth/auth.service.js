const bcrypt = require('bcrypt');
const userService = require('../user/user.service');
const logger = require('../../services/logger.service');
var nodemailer = require('nodemailer');

async function login(username, password) {
  logger.debug(`auth.service - login with username: ${username}`);

  const user = await userService.getByUsername(username);

  console.log("user", user);

  if (!user) return Promise.reject('Invalid username or password');
  const match = await bcrypt.compare(password, user.password);
  if (!match) return Promise.reject('Invalid username or password');

  delete user.password;
  user._id = user._id.toString();
  return user;
}

async function signup(username, password, fullname, imgUrl) {
  const saltRounds = 10;

  logger.debug(`auth.service - signup with username: ${username}, fullname: ${fullname}`);
  if (!username || !password || !fullname) return Promise.reject('fullname, username and password are required!');

  const existingUser = await userService.getByUsername(username);

  if (existingUser) return null
  else {
    const hash = await bcrypt.hash(password, saltRounds);
    const mailData = {
      from: 'powerfulmailer@gmail.com',  
      to: username,   
      subject: 'Welcome To Powerfull',
      text: 'powerful',
      html: '<b>Hello you created a profile </b> <br> Welcome to powerfull! <br/>',
    };

    const transporter = nodemailer.createTransport({
      port: 465,               // true for 465, false for other ports
      host: "smtp.gmail.com",
      auth: {
        user: 'powerfulmailer@gmail.com',
        pass: 'powerful2022',
      },
      secure: true,
    });


    // console.log("req.body ==============>", req.body);
// 
    // res.send(req.body);


    transporter.sendMail(mailData, function (err, info) {
      if (err) {
        console.log("b************", err)

        res.send(err);

      }
      else
        console.log(info);

      res.send(info);

    });


    return userService.add({ username, password: hash, fullname, imgUrl });
  }
}

module.exports = {
  signup,
  login,
};

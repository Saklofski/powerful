const express = require('express')


const { requireAuth } = require('../../middlewares/requireAuth.middleware')
const { log } = require('../../middlewares/logger.middleware')
const { addBoard, getBoards, deleteBoard, updateBoard, getById } = require('./board.controller')

// import * as mime from 'mime-types';
const { extension } = require('mime-types')
const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        let ext = extension(file.mimetype);

        cb(null, file.fieldname + '-' + uniqueSuffix + '.' + ext)
    }
})

const upload = multer({ storage: storage })




const router = express.Router()

router.get('/', log, requireAuth, getBoards)
router.post('/', log, requireAuth, addBoard)
router.put('/:id', log, requireAuth, updateBoard)
router.delete('/:id', requireAuth, deleteBoard)
router.get('/:id', requireAuth, getById)



router.post('/file', upload.single("file"), function (req, res, next) {


// console.log(req)
    res.send(`http://localhost:3030/uploads/${req.file.filename}`);
    // req.file.filename




    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    // return res
    // res.send(req)


})







module.exports = router
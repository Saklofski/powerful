import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { toggleMenu } from '../../store/actions/system.actions';

import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuList from '@mui/material/MenuList';
import { onUpdateUser } from '../../store/actions/user.actions';

class _PopoverMenu extends React.Component {
  state = {};

  onClose = (event) => {
    if (this.props.anchor && this.props.anchor.contains(event.target)) {
      return;
    }
    // if (this.props.menu.id === 'notification') {
    //   const { user, onUpdateUser } = this.props;

    //   if (user.notifications.filter(item => !item.isRead).length) {
    //   console.log("notifications unread");

    //     const notifications = user.notifications.map(notification => { return { ...notification, isRead: true } });
    //     onUpdateUser({ ...user, notifications })

    //   }

    //   // console.log(user, this.props.menu.id);

    // }


    this.props.toggleMenu(false);
  };

  render() {
    const { isOpen, id, anchor } = this.props.menu;
    if (!isOpen || id !== this.props.id) return <></>;
    return (
      <Popper
        className={this.props.classNames}
        open={isOpen}
        anchorEl={anchor}
        role={undefined}
        placement={this.props.placement || "bottom-start"}
        transition
        disablePortal>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: placement === 'bottom-start' ? 'left top' : 'right top',
            }}>
            <Paper>
              <ClickAwayListener onClickAway={this.onClose}>
                <MenuList
                  autoFocusItem={isOpen}
                  id="composition-menu"
                  aria-labelledby="composition-button">
                  {this.props.header && <div className="popper-header">
                    {/* <button onClick={onBack}></button> */}
                    <div>{this.props.header}</div>
                    <button onClick={this.onClose}></button>
                  </div>}
                  {this.props.children}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    );
  }
}

const mapDispatchToProps = {
  toggleMenu,
  onUpdateUser

};

const mapStateToProps = state => {
  return {
    user: state.userModule.user,

    menu: state.systemModule.menu,
  };
};

export const PopoverMenu = connect(mapStateToProps, mapDispatchToProps)(withRouter(_PopoverMenu));

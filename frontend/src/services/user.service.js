import { httpService } from './http.service';
import { socketService } from './socket.service';
import _ from 'lodash';
import { cardService } from '../services/board-services/card.service';
import { utilService } from './util.service';


const STORAGE_KEY_LOGGEDIN_USER = 'loggedinUser';

const SOCKET_EVENT_SET_USER = 'set-user-socket';
const SOCKET_EVENT_UNSET_USER = 'unset-user-socket';

export const userService = {
  login,
  logout,
  signup,
  getLoggedinUser,
  getUsers,
  getById,
  update,
  sendMails
};

window.userService = userService;

function getUsers(filterBy) {
  return httpService.get(`user?name=${filterBy.name}`);
}

async function getById(userId) {
  const user = await httpService.get(`user/${userId}`);
  return user;
}

async function update(user, isCurrUser = true) {
  user = await httpService.put(`user/${user._id}`, user);
  return isCurrUser ? _saveLocalUser(user) : user;
}

async function login(userCred) {


  // console.log("$$$$$$$$$$$$$$$$$$$",userCred);
  // with service worker - use in production:
  const user = await httpService.post('auth/login', { ...userCred });
  console.log(user)
  // no service worker - use in developement:
  // const user = await httpService.post('auth/login', userCred);
  socketService.emit(SOCKET_EVENT_SET_USER, user._id);
  if (user) return _saveLocalUser(user);
}






function _DynamicMail(createdBy, card, type, values, board) {

  let cardUrl
  if (type !== 'invite') {
    cardUrl = type ? `http://localhost:3000/board/${board._id}/card/${card.id}` : '/';

  }

  const BASE_URL = process.env.NODE_ENV === 'production' ? '/api/' : '//localhost:3030/api/';

  // let boardUrl=`http://localhost:3000/board/${board._id}`;
  // const CardLink = () => <Link to={cardUrl} className="link">{card.title}</Link>;

  /* const key = 'AIzaSyDgw0mWmcS4OoFUyLUj5oNbfo4KGzpHiYA'; */
  switch (type) {
    case 'ADD-CARD':
      return `<span>added <a href="${cardUrl}">${card.title}</a> to ${values.listTitle}</span>`;
    case 'UPDATE-TITLE':
      return `<span>Updated title on card <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-MEMBER':
      if (values.member._id === createdBy._id) return `<span>joined to <a href="${cardUrl}">${card.title}</a></span>`;
      return `<span>added <span className="created-by">${values.member.fullname || values.member.username}</span> to <a href="${cardUrl}">${card.title}</a></span>`;
    case 'REMOVE-MEMBER':
      if (values.member._id === createdBy._id) return `<span>left <a href="${cardUrl}">${card.title}</a></span>`;
      return `<span>removed <span className="created-by">${values.member.fullname || values.member.username}</span> from <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-CHECKLIST':
      return `<span>added checklist "${values.title}" to <a href="${cardUrl}">${card.title}</a></span>`;
    case 'CHECKLIST-COMPLETE':
      return `<span>completed "${values.title}" on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'CHANGE-DESCRIPTION':
      return `<span>changed the description of <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-ITEM-TO-CHECK-LIST':
      return `<span>added an item to check list ${values.title} on   <a href="${cardUrl}">${card.title}</a></span>`;

    case 'DELETE-CHECK-LIST':
      return `<span>deleted checklist ${values.title} on <a href="${cardUrl}">${card.title}</a> </span>`;

    case 'REMOVE-ITEM-FROM-CHECK-LIST':
      return `<span>removed item "${values.removedItemtitle}"  of checklist ${values.checkListTitle} on  <a href="${cardUrl}">${card.title}</a></span>`;


    case 'UPDATE-ITEM-FROM-CHECK-LIST':
      return `<span>changed item ${values.updatedItemtitle}  of checklist ${values.checkListTitle} on  <a href="${cardUrl}">${card.title}</a></span>`;


    case 'UPDATE-CHECK-LIST':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>updated checklist ${values.title} on  <a href="${cardUrl}">${card.title}</a></span>`;


    /* case 'ADD-LOCATION':
      const { lat, lng } = values.location;
      return <div>
        <div>added location {values.title} to <a href="${cardUrl}">${card.title}</a></div>
        <img src={`https://maps.googleapis.com/maps/api/staticmap?key=${ key }&libraries=places&zoom=13&size=225x66&markers=icon%3Ahttps://trello.com/images/location-marker.png%7C${ lat },${ lng }`}
          alt='card-location' />
      </div>; */

    case 'REMOVE-ATTACHMENT':
      return `<span>removed an attachment from  <a href="${cardUrl}">${card.title}</a></span>`;

    case 'UPDATE-ATTACHMENT':
      return `<span>updated an attachment on  <a href="${cardUrl}">${card.title}</a></span>`;

    // UPDATE-ATTACHMENT
    case 'ADD-ATTACHMENT':
      // const isValidImg = utilService.isValidImg(values.attachment.url);
      const AttachmentName = _.truncate(values.attachment.name, { length: 20 }) || _.truncate(values.attachment.url, { length: 20 });
      return `<div>
        <div>attached 
           <a className="link" target="_blank" rel="noreferrer" download href=${values.attachment.url}>${AttachmentName} </a>
          to <a href="${cardUrl}">${card.title}</a></div>
          </div>`;
    case 'ADD-DUE-DATE':
      return ` <span>set the due date on <a href="${cardUrl}">${card.title}</a> to 
        <span className={due-date ${cardService.checkDueDate({ date: values.date, isComplete: false })}}>
          <span className="due-date-icon"></span>
          <span>${utilService.getFormattedDate(values.date, true)}</span>
        </span>
      </span>`;
    case 'MARK-DUE-DATE':
      return `<span>marked the due date on <a href="${cardUrl}">${card.title}</a> ${values.dueDate.isComplete ? 'complete' : 'incomplete'} </span>`;
    case 'ADD-COVER':
      return `<div>set cover image on <a href="${cardUrl}">${card.title}</a></div>`;
    case 'SET-COLOR-COVER':
      return `<div>set ${values.color} as a cover color  on <a href="${cardUrl}">${card.title}</a></div>`;


    case 'REMOVE-COVER':
      return `<span>removed cover image on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-COMMENT':
      return `<span>commented on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'ADD-LABEL':
      return `<span>added label ${values.label.title}on <a href="${cardUrl}">${card.title}</a></span>`;
    case 'REMOVE-LABEL':
      return `<span>removed label ${values.label.title} from <a href="${cardUrl}">${card.title}</a></span>`;
    case 'invite':
      return `<span>invited you to  join Board <a href="http://localhost:3000/invite/${board._id}">${board.title}</a> !</span>`;

    case 'ARCHIVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>moved <a href="${cardUrl}">${card.title}</a>  to archive </span>`;



    case 'UNARCHIVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>unarchived <a href="${cardUrl}">${card.title}</a>  </span>`;

    case 'REMOVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>removed ${card.title}</span>`;
    case 'DELETE-COMMENT':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>removed his comment on <a href="${cardUrl}">${card.title}</a></span>`;

    case 'UPDATE-COMMENT':
      // console.log(currentUser);
      return `<span>updated his comment on <a href="${cardUrl}">${card.title}</a></span>`;


    case 'COPY-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      return `<span>copied ${card.title}  in list ${values.listTitle}</span>`;
    case 'MOVE-CARD':
      // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
      // return <span>{`moved ${notification.card.title}` to}   </span>;
      return `<span>moved <a href="${cardUrl}">${card.title}</a> to list ${values.listTitle}   </span>`;



    default:



      return `<span></span>`;
  }
}


async function sendMails(createdBy, card, type, values, board, userToUpdate = null) {

  // console.log("sendMails",createdBy, card, type, values, board)

  // "from": "swallowmailer@gmail.com",
  // "to": [
  //     "shawkifitouri@gmail.com",
  //     "shawkifitouri1@gmail.com"
  // ],
  // "subject": "Sending Email using Node.js",
  // "text": "That was easy!",
  // "html": "<b>Hey there! </b> <br> This is our first message sent with Nodemailer<br/>"
  // console.log(" sendMails card.members",card.members);
  if (type === 'invite' && userToUpdate) {

    const mailData = {
      from: 'swallowmailer@gmail.com',
      to: userToUpdate.username,
      subject: 'board Invitation',
      text: 'That was easy!',
      html: createdBy.fullname + " " + _DynamicMail(createdBy, card, type, values, board),
    }


    // mailData.to = card.members.map(member => { return member.username });

    const mailResponse = await httpService.post('user/sendmail/members', { ...mailData });

  } else {
    const mailData = {
      from: 'powerfulmailer@gmail.com',
      to: [],
      subject: type === 'invite' ? 'board Invitation' : 'update',
      text: 'powerful',
      html: createdBy.fullname + " " + _DynamicMail(createdBy, card, type, values, board),
    }


    mailData.to = card.members.map(member => { return member.username }).filter(member => member !== createdBy.username);

    const mailResponse = await httpService.post('user/sendmail/members', { ...mailData });
  }
}






async function signup(userCred) {
  const user = await httpService.post('auth/signup', userCred);
  socketService.emit(SOCKET_EVENT_SET_USER, user._id);
  return _saveLocalUser(user);
}

async function logout() {
  sessionStorage.removeItem(STORAGE_KEY_LOGGEDIN_USER);
  socketService.emit(SOCKET_EVENT_UNSET_USER);
  return await httpService.post('auth/logout');
}

function _saveLocalUser(user) {
  sessionStorage.setItem(STORAGE_KEY_LOGGEDIN_USER, JSON.stringify(user));
  return user;
}

function getLoggedinUser() {
  return JSON.parse(sessionStorage.getItem(STORAGE_KEY_LOGGEDIN_USER) || 'null');
}

(async () => {
  var user = getLoggedinUser();
  if (user) socketService.emit(SOCKET_EVENT_SET_USER, user._id);
})();
